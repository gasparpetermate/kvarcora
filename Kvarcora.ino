#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

// Software SPI (slower updates, more flexible pin options):
// pin D5 - Serial clock out (SCLK)
// pin D7 - Serial data out (DIN)
// pin D1 - Data/Command select (D/C)
// pin D8 - LCD chip select (CS)
// pin D2 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544( D5, D7, D1, D8, D2);
struct ido {
  public:
    int h;
    int m;
    int s;
    void set(unsigned long milli);
    void set(unsigned long h, unsigned long m, unsigned long s) {
      this->h = h;
      this->m = m;
      this->s = s;
    }
};

void ido::set(unsigned long milli) {
  milli /= 1000;
  int tmp = milli % 3600;
  int h = milli / 3600;
  int s = tmp % 60;
  int m = tmp / 60;
  set(h, m, s);
}

void printtime(ido now) {
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(BLACK);
  display.setCursor(0,0);

   
    
    display.printf("%02d\n%02d\n%02d",now.h,now.m,now.s);
    //if ((i > 0) && (i % 14 == 0))
      //display.println();
      
  display.display();
}

ido actual;
unsigned long start;

void setup()
{
  start = millis();
  display.begin();
	
}

void loop()
{
  actual.set(millis()-start);
	printtime(actual);
}
